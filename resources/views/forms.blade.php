@extends('layouts.default')
{{-- Page title --}}
@section('title')
    Forms @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->
    <link href="{{ asset('vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />

    <!--end of page vendors -->
@stop
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <div aria-label="breadcrumb" class="card-breadcrumb">
            <h1>Forms</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Contenido</a></li>
                <li class="breadcrumb-item active" aria-current="page">Formularios</li>
            </ol>
        </div>
        <div class="separator-breadcrumb border-top"></div>




    </section>
    <!-- /.content -->
    <!-- /.content -->
    <section class="content">
        <div class="row">
            <!--row starts-->
            <div class="col-md-12 col-lg-6 col-sm-12 col-12">
                <!--lg-6 starts-->
                <!--basic form starts-->
                <div class="my-3">
                    <div class="card p-0" id="hidepanel1">
                        <div class="card-header bg-primary text-white">
                            <h3 class="card-title d-inline">
                                Forma básica 1
                            </h3>
                            <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" action="#">
                                <!-- CSRF Token -->
                                <!-- Name input-->
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="name">Nombre</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <input id="name" name="name" type="text" placeholder="Your name" class="form-control"></div>
                                    </div>
                                </div>
                                <!-- Email input-->
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="email">Tu correo electrónico</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <input id="email" name="email" type="text" placeholder="Your email" class="form-control"></div>
                                    </div>
                                </div>
                                <!-- Message body -->
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 col-lg-3 col-12 control-label" for="message">Tu mensaje</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                            <textarea class="form-control resize_vertical" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <!-- Form actions -->
                                <div class="form-position">
                                    <div class="row">
                                        <div class="col-md-12  col-sm-12 col-12  col-lg-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--basic form 2 starts-->
                <div class="my-3">
                    <div class="card p-0" id="hidepanel2">
                        <div class="card-header bg-info text-white">
                            <h3 class="card-title d-inline">
                                Forma básica 2
                            </h3>
                            <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" action="#">
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <fieldset>
                                    <!-- Name input-->
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 col-lg-3 col-12 control-label" for="name1">Dirección de correo electrónico</label>
                                            <div class="col-md-9 col-lg-9 col-12">
                                                <input id="name1" name="name" type="text" placeholder="Enter your Email" class="form-control"></div>
                                        </div>
                                    </div>
                                    <!-- Email input-->
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 col-lg-3 col-12 control-label" for="password">Contraseña</label>
                                            <div class="col-md-9 col-12 col-lg-9">
                                                <input id="password" name="password" type="password" placeholder="Enter your Password" class="form-control"></div>
                                        </div>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheckin">
                                            <label class="custom-control-label" for="customCheckin">Mantener la sesión iniciada</label>
                                        </div>
                                    </div>

                                    <!-- Form actions -->
                                    <div class="form-group">
                                        <div class="row my-2">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-9 col-lg-9 col-12">
                                                <button type="submit" class="btn btn-responsive btn-info">Registrarse</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!--panel body ends--> </div>
                </div>
                <!--input form starts-->
                <div class="my-3">
                    <div class="card p-0" id="hidepanel5">
                        <div class="card-header bg-warning text-white">
                            <h3 class="card-title d-inline">
                                Entradas de formulario
                            </h3>
                            <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="card-body">
                            <form role="form">
                                <div class="form-group input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">@</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="User name"></div>
                                <div class="form-group input-group">
                                    <input type="text" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text image_radius">.00</span>
                                    </div>
                                </div>
                                <div class="form-group input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-euro-sign"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Font Awesome Icon"></div>
                                <div class="form-group input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input type="text" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text Form image_radius">.00</span>
                                    </div>
                                </div>
                                <div class="form-group input-group">
                                    <input type="text" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-btn input-group-append">
                                            <button class="btn btn-default input-group-text image_radius" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail border" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                    <div>
                                            <span class="btn btn-default btn-file px-0">
                                                <span class="fileinput-new btn btn-secondary ">Seleccionar imagen</span>
                                                <span class="fileinput-exists btn btn-secondary">Cambio</span>
                                                <input type="file" name="..."></span>
                                        <a href="#" class="btn btn-secondary fileinput-exists color_a" data-dismiss="fileinput">Eliminar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--md-6 ends-->
            <div class="col-md-12 col-lg-6 col-12 my-3">
                <!--md-6 starts-->
                <!--form control starts-->
                <div class="card p-0" id="hidepanel6">
                    <div class="card-header bg-success text-white">
                        <h3 class="card-title d-inline text-white">
                            Controles de formulario
                        </h3>
                        <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
                    </div>
                    <div class="card-body">
                        <form role="form" id="form_controls">
                            <div class="form-group">
                                <label for="input">Entrada de texto</label>
                                <input class="form-control" id="input">
                                <p class="help-block">Ejemplo de texto de ayuda a nivel de bloque aquí.</p>
                            </div>
                            <div class="form-group">
                                <label for="input2">Entrada de texto con marcador de posición</label>
                                <input class="form-control" placeholder="Enter text" id="input2"></div>
                            <div class="form-group">
                                <label>Static Control</label>
                                <p class="form-control-static">email@example.com</p>
                            </div>
                            <div class="form-group">
                                <label for="area">Área de texto</label>
                                <textarea class="form-control resize_vertical" id="area" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Checkboxes</label>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Laravel</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2">Html</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                                    <label class="custom-control-label" for="customCheck3">Javascript</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="mr-30">Inline Checkboxes</label>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="incustomCheck1">
                                    <label class="custom-control-label" for="incustomCheck1">Php</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="incustomCheck2">
                                    <label class="custom-control-label" for="incustomCheck2">Bootstrap</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="incustomCheck3">
                                    <label class="custom-control-label" for="incustomCheck3">Jquery</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Radio Buttons</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Home</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Calender</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio3">Contact</label>
                                </div>

                                {{--<div class="radio">--}}
                                    {{--<label>--}}
                                        {{--<input type="radio" name="optionsRadios" class="custom-radio" id="optionsRadios3" value="option3"> Radio 3</label>--}}
                                {{--</div>--}}
                            </div>
                            <div class="form-group">
                                <label class="mr-30">Inline Radio Buttons</label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="incustomRadio1" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="incustomRadio1">Excellent</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="incustomRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="incustomRadio2">Good</label>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="select1">Seleccione</label>
                                <select class="form-control" id="select1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="multi">Varias selecciones</label>
                                <select multiple class="form-control" id="multi">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="multi">Selecciones personalizadas</label>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="multi">Entrada de archivo personalizado</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Elija el archivo</label>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-responsive btn-secondary">Botón de enviar</button>
                            <button type="reset" class="btn btn-responsive btn-secondary" id="reset">Botón de reinicio</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-6 col-12 my-3">
            <div class="card p-0">
            <div class="card-header bg-danger text-white">
                <h3 class="card-title d-inline text-white">
                    Estado de validación
                </h3>
                <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
            </div>
                <div class="card-body">
            <form>
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="validationServer01">Nombre propio</label>
                        <input type="text" class="form-control is-valid" id="validationServer01" placeholder="First name" value="Mark" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="validationServer02">Apellido</label>
                        <input type="text" class="form-control is-valid" id="validationServer02" placeholder="Last name" value="Otto" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="validationServerUsername">Nombre de usuario</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend3">@</span>
                            </div>
                            <input type="text" class="form-control is-invalid" id="validationServerUsername" placeholder="Username" aria-describedby="inputGroupPrepend3" required>
                            <div class="invalid-feedback">
                                Por favor, elija un nombre de usuario.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="validationServer03">Ciudad</label>
                        <input type="text" class="form-control is-invalid" id="validationServer03" placeholder="City" required>
                        <div class="invalid-feedback">
                            Proporcione una ciudad válida.
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="validationServer04">Estado</label>
                        <input type="text" class="form-control is-invalid" id="validationServer04" placeholder="State" required>
                        <div class="invalid-feedback">
                            Proporcione un estado válido.
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="validationServer05">Zip</label>
                        <input type="text" class="form-control is-invalid" id="validationServer05" placeholder="Zip" required>
                        <div class="invalid-feedback">
                            Proporcione un estado un zip válido.
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" required>
                        <label class="form-check-label" for="invalidCheck3">
                            Acepta los términos y condiciones
                        </label>
                        <div class="invalid-feedback">
                            Debes estar de acuerdo antes de enviar.
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Enviar formulario</button>
            </form>
                </div>
            </div>
            </div>
            <div class="col-md-12 col-lg-6 col-12 my-3">
                <div class="card p-0" id="hidepanel3">
                    <div class="card-header bg-secondary ">
                        <h3 class="card-title d-inline">
                            Fieldsets deshabilitados
                        </h3>
                        <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
                    </div>
                    <div class="card-body">
                        <form role="form">
                            <fieldset disabled>
                                <div class="form-group">
                                    <label for="disabledSelect">Entrada inhabilitada</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="Disabled input" disabled></div>
                                <div class="form-group">
                                    <label for="disabledSelect">Menú de selección desactivado</label>
                                    <select id="disabledSelect" class="form-control" disabled>
                                        <option disabled>Selección de discapacitados</option>
                                    </select>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="disdefaultCheck" disabled>
                                    <label class="form-check-label" for="disdefaultCheck">
                                        Casilla de verificación deshabilitada
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="disexampleRadios" value="option3" disabled>
                                    <label class="form-check-label" for="disexampleRadios">
                                        Radio para discapacitados
                                    </label>
                                </div>
                                {{--<div class="custom-control custom-checkbox">--}}
                                    {{--<input type="checkbox" class="custom-control-input" id="customCheckdisable">--}}
                                    {{--<label class="custom-control-label text-light" for="customCheckdisable">&ensp;Disabled Custom Checkbox</label>--}}
                                {{--</div>--}}

                                {{--<div class="custom-control custom-radio">--}}
                                    {{--<input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" disabled>--}}
                                    {{--<label class="custom-control-label" for="customRadioDisabled2">Disabled Custom radio</label>--}}
                                {{--</div>--}}
                                <button type="submit" class="btn btn-responsive my-2 btn-secondary">Disabled Button</button>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <div class="card p-0" id="hidepanel2">
                    <div class="card-header bg-success text-white">
                        <h3 class="card-title d-inline">
                            Formulario básico con archivo
                        </h3>
                        <span class="float-right">
                                    <i class="fa fa-chevron-up clickable"></i>
                                    <i class="fa fa-times removepanel clickable"></i>
                                </span>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" action="#">
                            <!-- CSRF Token -->
                            <!-- Name input-->
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 col-lg-3 col-12 control-label" for="name122">Nombre</label>
                                    <div class="col-md-9 col-lg-9 col-12">
                                        <input id="name122" name="name" type="text" placeholder="Your name" class="form-control"></div>
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 col-lg-3 col-12 control-label" for="email1">Tu correo electrónico</label>
                                    <div class="col-md-9 col-lg-9 col-12">
                                        <input id="email1" name="email" type="text" placeholder="Your email" class="form-control"></div>
                                </div>
                            </div>
                            <!-- Message body -->
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 col-lg-3 col-12 control-label" for="message1">Tu mensaje</label>
                                    <div class="col-md-9 col-lg-9 col-12">
                                        <textarea class="form-control resize_vertical" id="message1" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                                    </div>
                                </div>
                            </div>

                                <div class="form-group">
                                    <div class="row">
                                    <label class="col-md-3 col-lg-3 col-12 control-label" for="exampleFormControlFile1">Archivo de ejemplo</label>
                                        <div class="col-md-9 col-lg-9 col-12">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                        </div>
                                    </div>
                                </div>


                            <!-- Form actions -->
                            <div class="form-position">
                                <div class="row">
                                    <div class="col-md-12  col-sm-12 col-12  col-lg-12 text-right">
                                        <button type="submit" class="btn btn-responsive btn-success">Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>



    </section>



@stop
@section('footer_scripts')
    <!--   page level js ----------->
    <script src="{{ asset('vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('js/pages/formlayout.js') }}"></script>
    <!-- end of page level js -->
@stop
