@extends('layouts.default')

{{-- Page title --}}
@section('title')
Factura @parent
@stop

@section('content')
   <section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>{{ __('Editar') }} Factura</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="card">
           <div class="card-body">
                {!! Form::model($factura, ['route' => ['facturas.update', $factura->id], 'method' => 'patch','class' => 'form-horizontal']) !!}

                    @include('facturas.fields')

                {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection
