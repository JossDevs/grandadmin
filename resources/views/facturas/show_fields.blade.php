<tr>
    <th scopre="row">{!! Form::label('id', 'Id:') !!}</th>
    <td>{{ $factura->id }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('fecha_emision', 'Fecha Emision:') !!}</th>
    <td>{{ $factura->fecha_emision }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('estado', 'Estado:') !!}</th>
    <td>{{ $factura->estado }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('total', 'Total:') !!}</th>
    <td>{{ $factura->total }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('iva', 'Iva:') !!}</th>
    <td>{{ $factura->iva }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Fecha Creación', 'Fecha Creacion:') !!}</th>
    <td>{{ $factura->created_at }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('updated_at', 'Fecha Actualización:') !!}</th>
    <td>{{ $factura->updated_at }}</td>
</tr>


