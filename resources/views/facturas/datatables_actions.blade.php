{!! Form::open(['route' => ['facturas.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('facturas.show', $id) }}" class='btn btn-outline-primary btn-xs'><i class="im im-icon-Information"></i>
    </a>
    <a href="{{ route('facturas.edit', $id) }}" class='btn btn-outline-primary btn-xs'><i
                                class="im im-icon-File-Edit"></i>
    </a>
    {!! Form::button('<i class="im im-icon-Remove"></i>', [
        'type' => 'Enviar',
        'class' => 'btn btn-outline-danger btn-xs',
        'onclick' => "return confirm('Está Seguro?')"
    ]) !!}
</div>
{!! Form::close() !!}
