<!-- Fecha Emision Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('fecha_emision', 'Fecha Emision:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::text('fecha_emision', null, ['class' => 'form-control','id'=>'fecha_emision']) !!}
        </div>
    </div>
</div>

@section('footer_scripts')
<script type="text/javascript">
    $('#fecha_emision').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
</script>
@endsection


<!-- Estado Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('estado', 'Estado:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::text('estado', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div


<!-- Total Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('total', 'Total:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::number('total', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<!-- Iva Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('iva', 'Iva:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::text('iva', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('facturas.index') }}" class="btn btn-default">Cancel</a>
</div>
