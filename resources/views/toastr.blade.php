@extends('layouts.default')
{{-- Page title --}}
@section('title')
    Toastr @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->
    <link href="{{ asset('vendors/toastr/css/toastr.css') }}"  rel="stylesheet" type="text/css" />

    <!--end of page vendors -->
@stop
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <div aria-label="breadcrumb" class="card-breadcrumb">
            <h1>Toastr</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Contenido</a></li>
                <li class="breadcrumb-item active" aria-current="page">Toastr</li>
            </ol>
        </div>
        <div class="separator-breadcrumb border-top"></div>




    </section>
    <!-- /.content -->
    <!-- /.content -->
    <section class="content">
        <div class="row">
            <!--row starts-->
            <div class="col-12">
                <div class="card ">
                          <div class="card-body p-20">
                            <h5>Toastr</h5>
                            <p>Toastr es una biblioteca de Javascript para notificaciones sin bloqueo. Se requiere jQuery. El objetivo es crear una biblioteca básica simple que se pueda personalizar y ampliar. </p>
                            <div class="row">
                                <div class="col-12">
                                    <h6>Variantes</h6>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-success btn-block" id="toast-success">Éxito Toastr</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-warning btn-block" id="toast-warning">Advertencia Toastr</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-info btn-block" id="toast-info">Información Toastr</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-danger btn-block" id="toast-error">Peligro Toastr</button>
                                </div>
                            </div>
                         </div>
                </div>

                <div class="card ">
                          <div class="card-body p-20">

                            <div class="row">
                                <div class="col-12">
                                    <h6>Posiciones</h6>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-top-left">Arriba a la izquierda</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-top-center">Centro Superior</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-top-right">Parte superior derecha</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-top-full">Ancho superior completo</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-bottom-left">Abajo a la izquierda</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-bottom-right">Abajo a la derecha</button>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-bottom-center">Parte inferior central</button>
                                </div>
                                 <div class="col-md-6 col-12 mb-3">
                                    <button class="btn btn-outline-light btn-block text-dark" id="toast-position-bottom-full">Ancho total inferior</button>
                                </div>
                            </div>
                         </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-12 ">
                        <div class="card ">
                          <div class="card-body p-20 mb-3">


                                    <h6>Notificación de texto</h6>
                                    <div class="row">
                                         <div class=" col-12">
                                            <button class="btn btn-outline-info btn-block" id="toast-text-notification">Show Toast</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>


                        <div class="col-md-6 col-12 ">
                            <div class="card ">
                                <div class="card-body p-20">

                                        <h6>Botón de cierre</h6>
                                    <div class="row">
                                         <div class="col-12 mb-3">
                                            <button class="btn btn-outline-success btn-block" id="toast-close-button">Cerrar Toast</button>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>
                      </div>

                        <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="card ">
                          <div class="card-body p-20">


                                    <h6>Barra de progreso</h6>
                                    <div class="row">
                                         <div class=" col-12 mb-3">
                                            <button class="btn btn-outline-warning btn-block" id="toast-progress-bar">Show Toast</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>


                        <div class="col-md-6 col-12">
                            <div class="card ">
                                <div class="card-body p-20">

                                        <h6>Limpiar Toast</h6>
                                    <div class="row">
                                         <div class="col-12 mb-3">
                                            <button class="btn btn-outline-danger btn-block" id="toast-clear-btn">Limpiar Toast</button>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>
                      </div>

                       <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="card ">
                          <div class="card-body p-20">


                                    <h6>Eliminar Toast</h6>
                                    <p>quitar Toast sin animación</p>
                                    <div class="row">
                                         <div class=" col-12 mb-3">
                                            <button class="btn btn-outline-light text-dark btn-block mb-3" id="toast-show-remove">ver Toast</button>
                                             <button class="btn btn-outline-light text-dark btn-block" id="toast-remove">Eliminar Toast</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>


                        <div class="col-md-6 col-12">
                            <div class="card ">
                              <div class="card-body p-20">


                                    <h6>Clear Toast</h6>
                                    <p>Clear toast with animation</p>
                                    <div class="row">
                                         <div class=" col-12 mb-3">
                                            <button class="btn btn-outline-light text-dark btn-block mb-3" id="toast-show-clear">Show Toast</button>
                                             <button class="btn btn-outline-light text-dark btn-block" id="toast-clear">Clear Toast</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                      </div>

                      <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="card ">
                          <div class="card-body p-20">


                                    <h6>Show .6s</h6>
                                    <p>el tiempo del espectáculo se puede definir usando <span class="text-danger">showDuration</span></p>
                                    <div class="row">
                                         <div class=" col-12 ">
                                            <button class="btn btn-outline-info btn-block mb-3" id="toast-fast-duration">Show Fast Toast</button>

                                        </div>
                                    </div>

                                       <h6>Se acabó el tiempo 6s</h6>
                                    <p>El tiempo de espera se puede definir mediante <span class="text-danger">timeout</span> para establecer la cantidad de tiempo que se quedará</p>
                                    <div class="row">
                                         <div class=" col-12 ">
                                            <button class="btn btn-outline-info btn-block mb-3" id="toast-timeout">Se acabó el tiempo Toast</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>


                        <div class="col-md-6 col-12">
                            <div class="card ">
                              <div class="card-body p-20">


                                    <h6>Hide 3s</h6>
                                    <p>Ocultar se puede definir por  <span class="text-danger">hideDuration</span> para establecer la cantidad de tiempo que se tardará en ocultar el mensaje</p>
                                    <div class="row">
                                         <div class=" col-12">
                                            <button class="btn btn-outline-light text-dark btn-block mb-3" id="toast-slow-duration">Esconder Toast</button>

                                        </div>
 </div>
                                                <h6>Sticky</h6>
                                    <p>Se puede crear un mensaje fijo configurando el <span class="text-danger">se acabó el tiempo</span> to set <span class="text-danger">0</span></p>
                                    <div class="row">
                                         <div class=" col-12 ">
                                            <button class="btn btn-outline-light text-dark btn-block" id="toast-sticky">Sticky Toast</button>

                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                      </div>

                      <div class="card">

                            <div class="col-12 my-3">
                                <h6>Show / Hide animation</h6>
                                </div>
                          <div class="col-12">
                      <div class="row">

                    <div class="col-md-6 col-12">
                        <div class="card ">
                          <div class="card-body p-20">


                                    <h6>Fundirse /Desaparecer</h6>

                                    <div class="row">
                                         <div class=" col-12">
                                            <button class="btn btn-outline-success btn-block mb-3" id="toast-fade">Desvanecerse Toast</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>


                        <div class="col-md-6 col-12">
                            <div class="card ">
                              <div class="card-body p-20">


                                    <h6>Bajar deslizándose /Deslizar hacia arriba</h6>

                                    <div class="row">
                                         <div class=" col-12 ">
                                            <button class="btn btn-outline-success btn-block mb-3" id="toast-slide">Diapositiva Toast</button>

                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                      </div>
                           </div>
                      </div>




            </div>
            <!--md-6 ends-->

        </div>



    </section>



@stop
@section('footer_scripts')
    <!--   page level js ----------->
    <script src="{{ asset('vendors/toastr/js/toastr.js') }}" ></script>
    <script src="{{ asset('js/pages/toastr.js') }}"></script>
    <!-- end of page level js -->

@stop
