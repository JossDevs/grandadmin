@extends('layouts.default')
{{-- Page title --}}
@section('title')
    Typography @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->


    <!--end of page vendors -->
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div aria-label="breadcrumb" class="card-breadcrumb">
            <h1>Cards</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Contenido</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tipografía</li>
            </ol>
        </div>
    </section>
    <!-- /.content -->
    <section class="content typography">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="card">
                    <h4 class="card-title"><strong>Misc</strong></h4>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-7 col-12">
                                <h5>Texto truncado</h5>
                                <p class="text-truncate">Solicitar <code class="code-bold">.text-truncate</code> a cualquier elemento de texto para recortar su texto con elipse si es mayor que el ancho de línea.</p>

                            </div>
                            <div class="col-lg-5 col-12 code-preview ">
                                <p class="col-5 text-truncate">Texto</p>
                                <div class="card-footer border-top-0">
                                &lt;div class="text-truncate"&gt;Your long text&lt;/div&gt;
                                </div>

                            </div><br>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 col-12 mt-3">
                    <h5>Font Size
                    </h5>
                     <p>Solicitar <code class="code-bold">.fs-{value}</code> to cualquier elemento, donde<code>{value}</code> puede ser cualquiera de: </p>
                        <ul  class="ul-tag">
                            <li>8 9 10 11 12 13 14 15 16 17 18 19 </li>
                            <li>20 22 24 25 26 28</li>
                            <li>30 35 40 45 50</li>
                            <li>60 70 80 90;</li>
                        </ul>
                        </div>



                        <div class="col-lg-6 col-12 mt-3">
                            <div class="card-body">
                                <table class="table table-borderless border border-secondary text-center">
                                    <tr>
                                        <td><p class="fs-10">tamaño de fuente 10px</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="fs-10"&gt;</code>Font Size 10px<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="fs-18">Tamaño de fuente 18px</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="fs-18"&gt;</code>Font Size 18px<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="fs-26">Tamaño de fuente 26px</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="fs-26"&gt;</code>Font Size 26px<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>

                                </table>
                            </div>


                        </div>
                    </div>

                </div>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 col-12 mt-3">
                            <h5>Font weight
                            </h5>
                            <p>Solicitar <code class="code-bold">.fw-{value}</code> a cualquier elemento, donde <code>{value}</code> can be any of: 100, 200, 300, 400, 500, 600, 700, 800, 900</p>
                        </div>



                        <div class="col-lg-6 col-12 mt-3">
                            <div class="card-body">
                                <table class="table table-borderless border border-secondary text-center">
                                    <tr>
                                        <td><p class="lead fw-100">Peso de fuente 100</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lead fw-100"&gt;</code>Font Weight 100<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lead fw-300">Peso de fuente 300</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lead fw-300"&gt;</code>Font Weight 300<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lead fw-500">Peso de fuente 500</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lead fw-500"&gt;</code>Font Weight 500<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lead fw-700">Peso de fuente 700</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lead fw-700"&gt;</code>Font Weight 700<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lead fw-900">Peso de fuente 900</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lead fw-900"&gt;</code>Font Weight 900<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>


                        </div>
                    </div>

                </div>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 col-12 mt-3">
                            <h5>Line height
                            </h5>
                            <p>Solicitar <code class="code-bold">.lh-{size}</code>a cualquier elemento, donde <code>{size}</code> puede ser cualquiera de:</p>
                            <ul class="ul-tag">
                                <li>0, 1, 2, 3, 4, 5</li>
                                <li>15, 25, 35, 45</li>
                                <li>11, 12, 13, 14, 15, 16, 17, 18, 19</li>
                                <li>22, 24, 26, 28</li>
                            </ul>
                            <p>Para comprender mejor los valores, eche un vistazo a la definición de clase de algunos de ellos:</p>
                            <ul class="ul-tag">
                                <li><code>.lh-1  { line-height: 1; }</code></li>
                                <li><code>.lh-15 { line-height: 1.5; }</code></li>
                                <li><code>.lh-24 { line-height: 2.4; }</code></li>
                            </ul>
                        </div>



                        <div class="col-lg-6 col-12 mt-3">
                            <div class="card-body">
                                <table class="table table-borderless border border-secondary ">
                                    <tr>
                                        <td><p class="lh-1">Altura de línea 1</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lh-1"&gt;</code>altura de la línea 1<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lh-18">Altura de la línea 1.8</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lh-1.8"&gt;</code>altura de la línea 1.8<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lh-2">Altura de la línea 2</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lh-2"&gt;</code>altura de la línea 2<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p class="lh-25">Altura de la línea 2.5</p></td>
                                        <td class="bg-secondary"><div><code>&lt;p class="lh-2.5"&gt;</code>altura de la línea 2.5<code>&lt;/p&gt;</code></div>
                                        </td>
                                    </tr>

                                </table>
                            </div>


                        </div>
                    </div>

                </div>


            </div>

        </div>

    </section>

@stop
@section('footer_scripts')
    <!--   page level js ----------->

    <!-- end of page level js -->
@stop
