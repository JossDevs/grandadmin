@extends('layouts.default')
{{-- Page title --}}
@section('title')
    Pagination @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->

    <!--end of page vendors -->
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">

        <div aria-label="breadcrumb" class="card-breadcrumb">
            <h1>Pagination</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Contenido</a></li>
                <li class="breadcrumb-item active" aria-current="page">Paginación</li>
            </ol>
        </div>
        <div class="separator-breadcrumb border-top"></div>


    </section>
    <!-- content start-->
    <section class="content">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Paginación básica</h5>
                        <p>Paginación bordeada predeterminada</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previo</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Paginación con iconos</h5>
                        <p>Uso de la paginación con iconos</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Estados desactivados y activos</h5>
                        <p>Estados desactivados y activos</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previo</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active" aria-current="page">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Siguiente</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        {{--<div class="row">--}}
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Posición izquierda</h5>
                        <p>Paginación alineada a la izquierda</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-start">
                                <li class="page-item"><a class="page-link" href="#">Previo</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Posición central</h5>
                        <p>Paginación alineada al centro</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><a class="page-link" href="#">Previo</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Posición correcta</h5>
                        <p>Paginación alineada a la derecha</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item"><a class="page-link" href="#">Previo</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>



        {{--</div>--}}
        {{--<div class="row">--}}
            <div class="col-md-6 col-lg-6 col-xl-4 col-12">
                <div class="card">
                    <div class="card-header"><h5>Tamaño de la paginación</h5>
                        <p> Agregar <code>.paginación-lg</code> or <code>.paginación-sm</code> para tamaños adicionales.</p>
                    </div>
                    <div class="card-body">
                        <nav aria-label="...">
                            <ul class="pagination pagination-lg">
                                <li class="page-item active" aria-current="page">
      <span class="page-link">
        1
        <span class="sr-only">(current)</span>
      </span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                            </ul>
                        </nav>
                        <nav class="mt-25" aria-label="...">
                            <ul class="pagination pagination-sm">
                                <li class="page-item active" aria-current="page">
                                      <span class="page-link">
                                        1
                                        <span class="sr-only">(current)</span>
                                      </span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>

        {{--</div>--}}
        </div>
    </section>
@stop

@section('footer_scripts')

@stop
