@extends('layouts.default')

{{-- Page title --}}
@section('title')
Atributos Productos @parent
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Atributos Productos</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>

<!-- Main content -->
<div class="content">
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="card">
        <section class="card-header">
            <h5 class="card-title d-inline">Atributos Productos</h5>
            <span class="float-right">
                <a class="btn btn-primary pull-right" href="{{ route('atributosProductos.create') }}">Añadir Nuevo</a>
            </span>
        </section>
        <div class="card-body">
            @include('atributos_productos.table')
        </div>
    </div>
    <div class="text-center">
        
        @include('adminlte-templates::common.paginate', ['records' => $atributosProductos])

    </div>
</div>
@endsection
