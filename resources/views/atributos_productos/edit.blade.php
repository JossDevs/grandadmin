@extends('layouts.default')

{{-- Page title --}}
@section('title')
Atributos Producto @parent
@stop

@section('content')
   <section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>{{ __('Editar') }} Atributos Producto</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="card">
           <div class="card-body">
                {!! Form::model($atributosProducto, ['route' => ['atributosProductos.update', $atributosProducto->id], 'method' => 'patch','class' => 'form-horizontal']) !!}

                    @include('atributos_productos.fields')

                {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection
