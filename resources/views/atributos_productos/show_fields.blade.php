<tr>
    <th scopre="row">{!! Form::label('id', 'Id:') !!}</th>
    <td>{{ $atributosProducto->id }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Nombre', 'Nombre:') !!}</th>
    <td>{{ $atributosProducto->Nombre }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Slug', 'Slug:') !!}</th>
    <td>{{ $atributosProducto->Slug }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('TipoProductoId', 'Tipoproductoid:') !!}</th>
    <td>{{ $atributosProducto->TipoProductoId }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('OrdenProductoId', 'Ordenproductoid:') !!}</th>
    <td>{{ $atributosProducto->OrdenProductoId }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Fecha Creación', 'Fecha Creacion:') !!}</th>
    <td>{{ $atributosProducto->created_at }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('updated_at', 'Fecha Actualización:') !!}</th>
    <td>{{ $atributosProducto->updated_at }}</td>
</tr>


