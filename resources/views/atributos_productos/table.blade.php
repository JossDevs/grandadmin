<div class="table-responsive">
    <table class="table" id="atributosProductos-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Nombre</th>
        <th>Slug</th>
        <th>Tipoproductoid</th>
        <th>Ordenproductoid</th>
        <th>Fecha Creacion</th>
        <th>Fecha Actualización</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($atributosProductos as $atributosProducto)
            <tr>
                <td>{{ $atributosProducto->id }}</td>
            <td>{{ $atributosProducto->Nombre }}</td>
            <td>{{ $atributosProducto->Slug }}</td>
            <td>{{ $atributosProducto->TipoProductoId }}</td>
            <td>{{ $atributosProducto->OrdenProductoId }}</td>
            <td>{{ $atributosProducto->created_at }}</td>
            <td>{{ $atributosProducto->updated_at }}</td>
                <td>
                    {!! Form::open(['route' => ['atributosProductos.destroy', $atributosProducto->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('atributosProductos.show', [$atributosProducto->id]) }}" class='btn btn-outline-primary btn-xs'><i class="im im-icon-Information"></i></a>
                        <a href="{{ route('atributosProductos.edit', [$atributosProducto->id]) }}" class='btn btn-outline-primary btn-xs'><i
                                class="im im-icon-File-Edit"></i></a>
                        {!! Form::button('<i class="im im-icon-Remove"></i>', ['type' => 'submit', 'class' => 'btn btn-outline-danger btn-xs', 'onclick' => "return confirm('Está Seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
