@extends('layouts.default')

{{-- Page title --}}
@section('title')
Categoria Producto @parent
@stop

@section('content')
   <section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>{{ __('Editar') }} Categoria Producto</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="card">
           <div class="card-body">
                {!! Form::model($categoriaProducto, ['route' => ['categoriaProductos.update', $categoriaProducto->id], 'method' => 'patch','class' => 'form-horizontal']) !!}

                    @include('categoria_productos.fields')

                {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection
