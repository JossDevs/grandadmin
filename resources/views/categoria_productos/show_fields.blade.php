<tr>
    <th scopre="row">{!! Form::label('id', 'Id:') !!}</th>
    <td>{{ $categoriaProducto->id }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Nombre', 'Nombre:') !!}</th>
    <td>{{ $categoriaProducto->Nombre }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Slug', 'Slug:') !!}</th>
    <td>{{ $categoriaProducto->Slug }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Descripcion', 'Descripcion:') !!}</th>
    <td>{{ $categoriaProducto->Descripcion }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Fecha Creación', 'Fecha Creacion:') !!}</th>
    <td>{{ $categoriaProducto->created_at }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('updated_at', 'Fecha Actualización:') !!}</th>
    <td>{{ $categoriaProducto->updated_at }}</td>
</tr>


