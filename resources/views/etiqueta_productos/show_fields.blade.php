<tr>
    <th scopre="row">{!! Form::label('id', 'Id:') !!}</th>
    <td>{{ $etiquetaProducto->id }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Nombre', 'Nombre:') !!}</th>
    <td>{{ $etiquetaProducto->Nombre }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Slug', 'Slug:') !!}</th>
    <td>{{ $etiquetaProducto->Slug }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Fecha Creación', 'Fecha Creacion:') !!}</th>
    <td>{{ $etiquetaProducto->created_at }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('updated_at', 'Fecha Actualización:') !!}</th>
    <td>{{ $etiquetaProducto->updated_at }}</td>
</tr>


