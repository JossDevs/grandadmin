@extends('layouts.default')

{{-- Page title --}}
@section('title')
Etiqueta Producto @parent
@stop

@section('content')
   <section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>{{ __('Editar') }} Etiqueta Producto</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="card">
           <div class="card-body">
                {!! Form::model($etiquetaProducto, ['route' => ['etiquetaProductos.update', $etiquetaProducto->id], 'method' => 'patch','class' => 'form-horizontal']) !!}

                    @include('etiqueta_productos.fields')

                {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection
