@extends('layouts.default')
{{-- Page title --}}
@section('title')
Alerts @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('css/pages.css')}}" rel="stylesheet">

<!--end of page vendors -->
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Alertas</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Contenido</a></li>
            <li class="breadcrumb-item active" aria-current="page">Alertas</li>
        </ol>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>
<!-- /.content -->
<section class="content general-components">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header">Alerta predeterminada</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-12">
                            <div class="alert alert-primary fade show" role="alert">
                                <strong>alert!</strong> <span>Información.</span>
                            </div>
                            <div class="row">
                                <div class="col-12 card-class">
                                    <span data-color="alert-primary" class="color1 active bg-primary  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-success" class="color1  bg-success  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-warning" class="color1 bg-warning  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-info" class="color1  bg-info  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-danger" class="color1  bg-danger  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-light" class="color1  bg-light  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-dark" class="color1  bg-dark  d-inline-block"><i
                                            class="im im-icon-Yes"></i></span>
                                    <span data-color="alert-secondary"
                                        class="color1  bg-secondary  border-secondary d-inline-block"><i
                                            class="im im-icon-Yes"></i></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 alert-class mt-3"><span>add class="alert-primary"</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 ">
            <div class="card">
                <div class="card-header">
                    <h5>Alerta descartable</h5>
                    <p>Texto </p>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                                <strong>Secundario!</strong> addClass <span>&ensp;Alerta secundaria</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <strong>Primario!</strong> addClass <span>&ensp;alerta-primaria</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-success  alert-dismissible fade show" role="alert">
                                <strong>Éxito!</strong> addClass <span>&ensp;alerta-Éxito</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Advertencia!</strong> addClass <span>&ensp;alerta-Advertencia</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <strong>Información!</strong> addClass <span>&ensp;alerta-Información</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-danger  alert-dismissible fade show" role="alert">
                                <strong>Peligro!</strong> addClass <span>&ensp;alerta-Peligro</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-light  alert-dismissible fade show" role="alert">
                                <strong>Ligero!</strong> addClass <span>&ensp;alerta-Ligero</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-dark  alert-dismissible fade show" role="alert">
                                <strong>Oscuro!</strong> addClass <span>&ensp;alerta-Oscuro</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 ">
            <div class="card">
                <div class="card-header">
                    <h5>Alerta de esquema</h5>
                    <p>Texto</p>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="alert alert-secondary alert-outline-secondary" role="alert">
                                <strong>Secundario!</strong> addClass <span>&ensp;alerta-secundario</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-primary alert-outline-primary " role="alert">
                                <strong>Primario!</strong> addClass <span>&ensp;alerta-primario</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-success  alert-outline-success" role="alert">
                                <strong>Exito!</strong> addClass <span>&ensp;alerta-Exito</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-warning  alert-outline-warning " role="alert">
                                <strong>Advertencia!</strong> addClass <span>&ensp;alerta-Advertencia</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-info  alert-outline-info" role="alert">
                                <strong>Información!</strong> addClass <span>&ensp;alerta-Información</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-danger  alert-outline-danger" role="alert">
                                <strong>Peligro!</strong> addClass <span>&ensp;alerta-Peligro</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-light  alert-outline-light " role="alert">
                                <strong>Light!</strong> addClass <span>&ensp;alert-light</span>.

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-dark  alert-outline-dark alert-dismissible fade show" role="alert">
                                <strong>Oscuro!</strong> addClass <span>&ensp;alerta-Oscuro</span>.

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 ">
            <div class="card">
                <div class="card-header">
                    <h5>Esquema de alerta descartable</h5>
                    <p>texto</p>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="alert alert-secondary alert-outline-secondary alert-dismissible fade show"
                                role="alert">
                                <strong>Secundario!</strong> addClass <span>&ensp;Alerta-esquema-secundario</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-primary alert-outline-primary alert-dismissible fade show"
                                role="alert">
                                <strong>Primario!</strong> addClass <span>&ensp;Alerta-esquema-primario</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-success  alert-outline-success alert-dismissible fade show"
                                role="alert">
                                <strong>Exito!</strong> addClass <span>&ensp;alerta-esquema-éxito</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-warning  alert-outline-warning alert-dismissible fade show"
                                role="alert">
                                <strong>Advertencia!</strong> addClass <span>&ensp;alerta-esquema-advertencia</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-info  alert-outline-info alert-dismissible fade show" role="alert">
                                <strong>Información!</strong> addClass <span>&ensp;alerta-esquema-info</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-danger  alert-outline-danger alert-dismissible fade show"
                                role="alert">
                                <strong>Peligro!</strong> addClass <span>&ensp;alerta-contorno-peligro</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-light  alert-outline-light alert-dismissible fade show"
                                role="alert">
                                <strong>Light!</strong> addClass <span>&ensp;alert-outline-light</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-dark  alert-outline-dark alert-dismissible fade show" role="alert">
                                <strong>Oscuro!</strong> addClass <span>&ensp;Alerta-esquema-oscuro</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">

            <div class="card">
                <h5 class="card-header">Alertas descartables con iconos</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>Secondariu!</strong> addClass
                                    &ensp;alert-secondary</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span>
                                <span class="alert-text"><strong>Primario!</strong> addClass &ensp;alerta-primario</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>Éxito!</strong> addClass alerta-éxito</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>Advertencia!</strong> addClass &ensp;alerta-advertencia</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>Información!</strong> addClass &ensp;alerta-Información</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>Advertencia!</strong> addClass &ensp;alerta-Advertencia</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-light alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>ligero!</strong> addClass &ensp;alerta-ligero</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="alert alert-dark alert-dismissible fade show" role="alert">
                                <span class="im im-icon-Information mr-3 icon"></span><span
                                    class="alert-text"><strong>Oscuro!</strong> addClass &ensp;alerta-Oscuro</span>.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@stop
@section('footer_scripts')
<!--   page level js ----------->
<script>

</script>
<!-- end of page level js -->
@stop
