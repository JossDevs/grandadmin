@extends('layouts.default')

{{-- Page title --}}
@section('title')
Tipo Productos @parent
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>Tipo Productos</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
</section>

<!-- Main content -->
<div class="content">
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="card">
        <section class="card-header">
            <h5 class="card-title d-inline">Tipo Productos</h5>
            <span class="float-right">
                <a class="btn btn-primary pull-right" href="{{ route('tipoProductos.create') }}">Añadir Nuevo</a>
            </span>
        </section>
        <div class="card-body">
            @include('tipo_productos.table')
        </div>
    </div>
    <div class="text-center">
        
    </div>
</div>
@endsection
