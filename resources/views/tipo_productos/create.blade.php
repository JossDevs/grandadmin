@extends('layouts.default')

{{-- Page title --}}
@section('title')
Tipo Producto @parent
@stop

@section('content')
    <section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>{{ __('Crear Nuevo') }} Tipo Producto</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'tipoProductos.store','class' => 'form-horizontal']) !!}

                    @include('tipo_productos.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
