@extends('layouts.default')
{{-- Page title --}}
@section('title')
    Modals @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->

    <!--end of page vendors -->
@stop
{{-- Page content --}}
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div aria-label="breadcrumb" class="card-breadcrumb">
            <h1>Modals</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Contenido</a></li>
                <li class="breadcrumb-item active" aria-current="page">Modelos</li>
            </ol>
        </div>
        <div class="separator-breadcrumb border-top"></div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="card ">
                    <div class="card-header"><h5 class="text-primary my-2">Modelo básico</h5></div>
                    <hr>
                    <div class="card-body py-3">
                        <p>Alternar una demostración modal de trabajo haciendo clic en el botón de abajo. Se deslizará hacia abajo y se desvanecerá desde
                            la parte superior de la página.</p>
                    </div>

                    <div class="col-12 py-3">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Lanzamiento modal de demostración
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header mt-0">
                                        <h5 class="modal-title" id="exampleModalLabel">Título modal</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Es el cuerpo modal
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="button" class="btn btn-primary">Guardar cambios</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6 col-12">
                <div class="card ">
                    <div class="card-header"><h5 class="text-primary my-2">Modal largo de desplazamiento</h5></div>
                    <hr>
                    <div class="card-body py-3">
                        <p>Cuando los modales se vuelven demasiado largos para la ventana gráfica o el dispositivo del usuario, se desplazan independientemente de la página.</p>
                    </div>

                    <div class="col-12 py-3">

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                            Lanzamiento modal de demostración
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog  modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0" id="exampleModalLongTitle">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Texto</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-primary">Guardar cambios</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-md-6 col-12">
                <div class="card ">
                    <div class="card-header"><h5 class="text-primary my-2">Modal centrado verticalmente</h5></div>
                    <hr>
                    <div class="card-body py-3">
                        <p>Agregar <code>.modal-dialog-centered</code> to <code>.modal-dialog</code> para centrar verticalmente el modal.</p>
                    </div>

                    <div class="col-12 py-3">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                            Lanzamiento modal de demostración
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Titulo Modal</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       Texto
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-primary">Guardar Cambios</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="card ">
                    <div class="card-header"><h5 class="text-primary my-2">Modal verticalmente largo</h5></div>
                    <hr>
                    <div class="card-body py-3">
                        <p>Cuando los modales se vuelven demasiado largos para la ventana gráfica o el dispositivo del usuario, se desplazan independientemente de la página.</p>
                    </div>

                    <div class="col-12 py-3">

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                            Lanzamiento modal de demostración
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog  modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0" id="exampleModalLongTitle">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       <p>Texto.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-primary">Guardar Cambios</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
    <!-- /.content -->

@stop
@section('footer_scripts')
    <!-- end of page level js -->
@stop

