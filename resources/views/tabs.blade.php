@extends('layouts.default')
{{-- Page title --}}
@section('title')
    Tabs @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->

    <!--end of page vendors -->
@stop
{{-- Page content --}}
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div aria-label="breadcrumb" class="card-breadcrumb">
                <h1>Tabs</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Contenido</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pestañas</li>
                </ol>
            </div>
            <div class="separator-breadcrumb border-top"></div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="card ">
                        <div class="card-header text-primary">Pestañas básicas </div>
                        <hr>
                        <div class="col-12">
                            <ul class="nav nav-tabs pt-15" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                                </li>
                            </ul>
                            <div class="tab-content py-3" id="myTabContent">
                                <div class="tab-pane fade show active text-justify" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    Texto
                                </div>
                                <div class="tab-pane fade py-3 text-justify" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    Texto
                                </div>
                                <div class="tab-pane fade py-3 text-justify" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                   Texto
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header text-primary">Pestañas con iconos</div>
                        <hr>
                        <div class="col-12">
                            <ul class="nav nav-tabs pt-15" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="homeicon-tab" data-toggle="tab" href="#homeicon" role="tab" aria-controls="home" aria-selected="true">
                                        <i class="im im-icon-Home px-2"></i>
                                        Inicio
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profileicon-tab" data-toggle="tab" href="#profileicon" role="tab" aria-controls="profile" aria-selected="false">
                                        <i class="im im-icon-User px-2"></i>
                                        Perfil
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contacticon-tab" data-toggle="tab" href="#contacticon" role="tab" aria-controls="contact" aria-selected="false">
                                        <i class="im im-icon-Phone-3G px-2"></i>
                                        Contacto
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content py-3" id="myTabContenticon">
                                <div class="tab-pane fade show active text-justify" id="homeicon" role="tabpanel" aria-labelledby="home-tab">
                                    Texto
                                </div>
                                <div class="tab-pane fade py-3 text-justify" id="profileicon" role="tabpanel" aria-labelledby="profile-tab">
                                    Texto
                                </div>
                                <div class="tab-pane fade py-3 text-justify" id="contacticon" role="tabpanel" aria-labelledby="contact-tab">
                                    Texto
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 align-self-stretch d-flex">
                    <div class="card ">
                        <div class="card-header text-primary">Pestañas con menús desplegables</div>
                        <hr>
                        <div class="col-12">

                             <ul class="nav nav-tabs pt-15" id="dropdwonTab">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="homedropdown-tab" data-toggle="tab" href="#homedropdown" aria-controls="home" aria-expanded="true">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profiledropdown-tab" data-toggle="tab" href="#profiledropdown" aria-controls="profile" aria-expanded="false">Perfil</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                        Desplegable
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" id="dropdown1-tab" href="#dropdown1" data-toggle="tab" aria-controls="dropdown1" aria-expanded="true">@fat</a>
                                        <a class="dropdown-item" id="dropdown2-tab" href="#dropdown2" data-toggle="tab" aria-controls="dropdown2" aria-expanded="true">@mdo</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" aria-controls="about" aria-expanded="false">Acerca de</a>
                                </li>
                            </ul>
                            <div class="tab-content " id="dropdwonTabContent">
                                <div role="tabpanel" class="tab-pane active show py-3 text-justify" id="homedropdown" aria-labelledby="homedropdown-tab" aria-expanded="true">
                                    <p>Texto
                                    </p>
                                </div>
                                <div class="tab-pane py-3 text-justify" id="profiledropdown" role="tabpanel" aria-labelledby="profiledropdown-tab" aria-expanded="false">
                                    <p>Texto
                                    </p>
                                </div>
                                <div class="tab-pane py-3" id="dropdown1" role="tabpanel" aria-labelledby="dropdown1-tab" aria-expanded="true">
                                    <p>Texto
                                    </p>
                                </div>
                                <div class="tab-pane py-3 text-justify" id="dropdown2" role="tabpanel" aria-labelledby="dropdown2-tab" aria-expanded="false">
                                    <p>Texto
                                    </p>
                                </div>
                                <div class="tab-pane py-3 text-justify" id="about" role="tabpanel" aria-labelledby="about-tab" aria-expanded="false">
                                    <p>Texto
                                    </p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 align-self-stretch d-flex">
                    <div class="card ">
                        <div class="card-header text-primary">Pestañas con pastillas</div>
                        <hr>
                        <div class="col-12">
                            <ul class="nav nav-pills mb-3 pt-15" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card ">
                        <div class="card-header text-primary">Pestañas con pastillas verticales</div>
                        <hr>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-3 py-15">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</a>
                                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
                                        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </section>
        <!-- /.content -->

@stop
@section('footer_scripts')
    <!-- end of page level js -->
@stop

