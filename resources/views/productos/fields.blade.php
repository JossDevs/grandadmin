<!-- Nombre Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('Nombre', 'Nombre:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::text('Nombre', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div


<!-- Descripcion Field -->
<div class="form-group ">
    <div class="row">
        {!! Form::label('Descripcion', 'Descripcion:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::textarea('Descripcion', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<!-- Precio Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('Precio', 'Precio:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::number('Precio', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<!-- Impuesto Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('Impuesto', 'Impuesto:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            <label class="checkbox-inline">
                {!! Form::hidden('Impuesto', 0) !!}
                {!! Form::checkbox('Impuesto', '1', null) !!}
            </label>
        </div>
    </div>
</div>


<!-- Categoria Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('Categoria', 'Categoria:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::select('Categoria', ['' => ''], null, ['class' => 'form-control']) !!}
        </div>
    </div>
</>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('productos.index') }}" class="btn btn-default">Cancel</a>
</div>
