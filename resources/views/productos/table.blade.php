<div class="table-responsive">
    <table class="table" id="productos-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Precio</th>
        <th>Impuesto</th>
        <th>Categoria</th>
        <th>Fecha Creacion</th>
        <th>Fecha Actualización</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($productos as $producto)
            <tr>
                <td>{{ $producto->id }}</td>
            <td>{{ $producto->Nombre }}</td>
            <td>{{ $producto->Descripcion }}</td>
            <td>{{ $producto->Precio }}</td>
            <td>{{ $producto->Impuesto }}</td>
            <td>{{ $producto->Categoria }}</td>
            <td>{{ $producto->created_at }}</td>
            <td>{{ $producto->updated_at }}</td>
                <td>
                    {!! Form::open(['route' => ['productos.destroy', $producto->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('productos.show', [$producto->id]) }}" class='btn btn-outline-primary btn-xs'><i class="im im-icon-Information"></i></a>
                        <a href="{{ route('productos.edit', [$producto->id]) }}" class='btn btn-outline-primary btn-xs'><i
                                class="im im-icon-File-Edit"></i></a>
                        {!! Form::button('<i class="im im-icon-Remove"></i>', ['type' => 'submit', 'class' => 'btn btn-outline-danger btn-xs', 'onclick' => "return confirm('Está Seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
