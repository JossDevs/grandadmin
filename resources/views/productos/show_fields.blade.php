<tr>
    <th scopre="row">{!! Form::label('id', 'Id:') !!}</th>
    <td>{{ $producto->id }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Nombre', 'Nombre:') !!}</th>
    <td>{{ $producto->Nombre }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Descripcion', 'Descripcion:') !!}</th>
    <td>{{ $producto->Descripcion }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Precio', 'Precio:') !!}</th>
    <td>{{ $producto->Precio }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Impuesto', 'Impuesto:') !!}</th>
    <td>{{ $producto->Impuesto }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Categoria', 'Categoria:') !!}</th>
    <td>{{ $producto->Categoria }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('Fecha Creación', 'Fecha Creacion:') !!}</th>
    <td>{{ $producto->created_at }}</td>
</tr>


<tr>
    <th scopre="row">{!! Form::label('updated_at', 'Fecha Actualización:') !!}</th>
    <td>{{ $producto->updated_at }}</td>
</tr>


