<li class="{!! (Request::is('tipoProductos*') ? 'active' : '' ) !!}">
    <a href="{{ route('tipoProductos.index') }}">
        <span class="mm-text ">Tipo Productos</span>
        <span class="menu-icon"><i class="im im-icon-Structure"></i></span>
    </a>
</li>

<li class="{!! (Request::is('ordenProductos*') ? 'active' : '' ) !!}">
    <a href="{{ route('ordenProductos.index') }}">
        <span class="mm-text ">Orden Productos</span>
        <span class="menu-icon"><i class="im im-icon-Gift-Box"></i></span>
    </a>
</li>

<li class="{!! (Request::is('atributosProductos*') ? 'active' : '' ) !!}">
    <a href="{{ route('atributosProductos.index') }}">
        <span class="mm-text ">Atributos Productos</span>
        <span class="menu-icon"><i class="im im-icon-Italic-Text"></i></span>
    </a>
</li>

<li class="{!! (Request::is('etiquetaProductos*') ? 'active' : '' ) !!}">
    <a href="{{ route('etiquetaProductos.index') }}">
        <span class="mm-text ">Etiqueta Productos</span>
        <span class="menu-icon"><i class="im im-icon-Structure"></i></span>
    </a>
</li>





<li class="{!! (Request::is('categoriaProductos*') ? 'active' : '' ) !!}">
    <a href="{{ route('categoriaProductos.index') }}">
        <span class="mm-text ">Categoria Productos</span>
        <span class="menu-icon"><i class="im im-icon-Structure"></i></span>
    </a>
</li>

<li class="{!! (Request::is('productos*') ? 'active' : '' ) !!}">
    <a href="{{ route('productos.index') }}">
        <span class="mm-text ">Productos</span>
        <span class="menu-icon"><i class="im im-icon-Structure"></i></span>
    </a>
</li>

<li class="{!! (Request::is('facturas*') ? 'active' : '' ) !!}">
    <a href="{{ route('facturas.index') }}">
        <span class="mm-text ">Facturas</span>
        <span class="menu-icon"><i class="im im-icon-Structure"></i></span>
    </a>
</li>

