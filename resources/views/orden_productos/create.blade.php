@extends('layouts.default')

{{-- Page title --}}
@section('title')
Orden Producto @parent
@stop

@section('content')
    <section class="content-header">
    <div aria-label="breadcrumb" class="card-breadcrumb">
        <h1>{{ __('Crear Nuevo') }} Orden Producto</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'ordenProductos.store','class' => 'form-horizontal']) !!}

                    @include('orden_productos.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
