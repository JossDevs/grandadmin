<!-- Nombre Field -->
<div class="form-group">
    <div class="row">
        {!! Form::label('Nombre', 'Nombre:',['class'=>'col-md-3 col-lg-3 col-12 control-label']) !!}
        <div class="col-md-9 col-lg-9 col-12">
            {!! Form::text('Nombre', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ordenProductos.index') }}" class="btn btn-default">Cancel</a>
</div>
