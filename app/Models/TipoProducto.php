<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class TipoProducto
 * @package App\Models
 * @version June 11, 2021, 5:42 pm UTC
 *
 * @property string $Nombre
 */
class TipoProducto extends Model
{
    use SoftDeletes;

    public $table = 'tipoproductos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'Nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
