<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class AtributosProducto
 * @package App\Models
 * @version June 11, 2021, 5:48 pm UTC
 *
 * @property \App\Models\TipoProductos $tipoProductos
 * @property \App\Models\OrdenProducto $ordenProducto
 * @property string $Nombre
 * @property string $Slug
 * @property integer $TipoProductoId
 * @property integer $OrdenProductoId
 */
class AtributosProducto extends Model
{

    public $table = 'atributosproductos';
    



    public $fillable = [
        'Nombre',
        'Slug',
        'TipoProductoId',
        'OrdenProductoId'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'Slug' => 'string',
        'TipoProductoId' => 'integer',
        'OrdenProductoId' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function tipoProductos()
    {
        return $this->hasOne(\App\Models\TipoProductos::class, 'id', 'TipoProductoId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function ordenProducto()
    {
        return $this->hasOne(\App\Models\OrdenProducto::class, 'id', 'OrdenProductoId');
    }
}
