<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class EtiquetaProducto
 * @package App\Models
 * @version June 11, 2021, 5:50 pm UTC
 *
 * @property string $Nombre
 * @property string $Slug
 */
class EtiquetaProducto extends Model
{
    use SoftDeletes;

    public $table = 'etiquetaproductos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'Nombre',
        'Slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'Slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
