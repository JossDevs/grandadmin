<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Producto
 * @package App\Models
 * @version June 11, 2021, 6:45 pm UTC
 *
 * @property string $Nombre
 * @property string $Descripcion
 * @property number $Precio
 * @property boolean $Impuesto
 * @property integer $Categoria
 */
class Producto extends Model
{

    public $table = 'productos';
    



    public $fillable = [
        'Nombre',
        'Descripcion',
        'Precio',
        'Impuesto',
        'Categoria'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'Precio' => 'double',
        'Impuesto' => 'boolean',
        'Categoria' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
