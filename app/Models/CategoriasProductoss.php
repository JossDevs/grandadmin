<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class CategoriasProductoss
 * @package App\Models
 * @version June 11, 2021, 6:07 pm UTC
 *
 * @property string $Nombre
 * @property string $Slug
 * @property string $Descripcion
 */
class CategoriasProductoss extends Model
{

    public $table = 'categoriasproductosss';
    



    public $fillable = [
        'Nombre',
        'Slug',
        'Descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'Slug' => 'string',
        'Descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
