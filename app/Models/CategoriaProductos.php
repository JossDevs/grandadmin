<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class CategoriaProductos
 * @package App\Models
 * @version June 11, 2021, 6:24 pm UTC
 *
 * @property string $Nombre
 * @property string $Slug
 * @property string $Descripcion
 */
class CategoriaProductos extends Model
{
    use SoftDeletes;

    public $table = 'categoriaproductoss';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'Nombre',
        'Slug',
        'Descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'Slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
