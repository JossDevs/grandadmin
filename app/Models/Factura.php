<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class Factura
 * @package App\Models
 * @version June 11, 2021, 7:08 pm UTC
 *
 * @property string $fecha_emision
 * @property string $estado
 * @property number $total
 * @property string $iva
 */
class Factura extends Model
{
    use SoftDeletes;

    public $table = 'facturas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'fecha_emision',
        'estado',
        'total',
        'iva'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha_emision' => 'datetime',
        'estado' => 'string',
        'total' => 'double',
        'iva' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
