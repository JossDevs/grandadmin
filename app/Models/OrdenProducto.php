<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class OrdenProducto
 * @package App\Models
 * @version June 11, 2021, 5:45 pm UTC
 *
 * @property integer $Nombre
 */
class OrdenProducto extends Model
{
    use SoftDeletes;

    public $table = 'ordenproductos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'Nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
