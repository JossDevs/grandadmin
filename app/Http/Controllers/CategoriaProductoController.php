<?php

namespace App\Http\Controllers;

use App\DataTables\CategoriaProductoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCategoriaProductoRequest;
use App\Http\Requests\UpdateCategoriaProductoRequest;
use App\Models\CategoriaProducto;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CategoriaProductoController extends AppBaseController
{
    /**
     * Display a listing of the CategoriaProducto.
     *
     * @param CategoriaProductoDataTable $categoriaProductoDataTable
     * @return Response
     */
    public function index(CategoriaProductoDataTable $categoriaProductoDataTable)
    {
        return $categoriaProductoDataTable->render('categoria_productos.index');
    }

    /**
     * Show the form for creating a new CategoriaProducto.
     *
     * @return Response
     */
    public function create()
    {
        return view('categoria_productos.create');
    }

    /**
     * Store a newly created CategoriaProducto in storage.
     *
     * @param CreateCategoriaProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriaProductoRequest $request)
    {
        $input = $request->all();

        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::create($input);

        Flash::success('Categoria Producto Guardado.');

        return redirect(route('categoriaProductos.index'));
    }

    /**
     * Display the specified CategoriaProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            Flash::error('Categoria Producto No encontrado');

            return redirect(route('categoriaProductos.index'));
        }

        return view('categoria_productos.show')->with('categoriaProducto', $categoriaProducto);
    }

    /**
     * Show the form for editing the specified CategoriaProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            Flash::error('Categoria Producto No encontrado');

            return redirect(route('categoriaProductos.index'));
        }

        return view('categoria_productos.edit')->with('categoriaProducto', $categoriaProducto);
    }

    /**
     * Update the specified CategoriaProducto in storage.
     *
     * @param  int              $id
     * @param UpdateCategoriaProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriaProductoRequest $request)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            Flash::error('Categoria Producto No encontrado');

            return redirect(route('categoriaProductos.index'));
        }

        $categoriaProducto->fill($request->all());
        $categoriaProducto->save();

        Flash::success('Categoria Producto Actualizado.');

        return redirect(route('categoriaProductos.index'));
    }

    /**
     * Remove the specified CategoriaProducto from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            Flash::error('Categoria Producto No encontrado');

            return redirect(route('categoriaProductos.index'));
        }

        $categoriaProducto->delete();

        Flash::success('Categoria Producto Eliminado.');

        return redirect(route('categoriaProductos.index'));
    }
}
