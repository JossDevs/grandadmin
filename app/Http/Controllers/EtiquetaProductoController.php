<?php

namespace App\Http\Controllers;

use App\DataTables\EtiquetaProductoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEtiquetaProductoRequest;
use App\Http\Requests\UpdateEtiquetaProductoRequest;
use App\Models\EtiquetaProducto;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class EtiquetaProductoController extends AppBaseController
{
    /**
     * Display a listing of the EtiquetaProducto.
     *
     * @param EtiquetaProductoDataTable $etiquetaProductoDataTable
     * @return Response
     */
    public function index(EtiquetaProductoDataTable $etiquetaProductoDataTable)
    {
        return $etiquetaProductoDataTable->render('etiqueta_productos.index');
    }

    /**
     * Show the form for creating a new EtiquetaProducto.
     *
     * @return Response
     */
    public function create()
    {
        return view('etiqueta_productos.create');
    }

    /**
     * Store a newly created EtiquetaProducto in storage.
     *
     * @param CreateEtiquetaProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateEtiquetaProductoRequest $request)
    {
        $input = $request->all();

        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::create($input);

        Flash::success('Etiqueta Producto Guardado.');

        return redirect(route('etiquetaProductos.index'));
    }

    /**
     * Display the specified EtiquetaProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            Flash::error('Etiqueta Producto No encontrado');

            return redirect(route('etiquetaProductos.index'));
        }

        return view('etiqueta_productos.show')->with('etiquetaProducto', $etiquetaProducto);
    }

    /**
     * Show the form for editing the specified EtiquetaProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            Flash::error('Etiqueta Producto No encontrado');

            return redirect(route('etiquetaProductos.index'));
        }

        return view('etiqueta_productos.edit')->with('etiquetaProducto', $etiquetaProducto);
    }

    /**
     * Update the specified EtiquetaProducto in storage.
     *
     * @param  int              $id
     * @param UpdateEtiquetaProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEtiquetaProductoRequest $request)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            Flash::error('Etiqueta Producto No encontrado');

            return redirect(route('etiquetaProductos.index'));
        }

        $etiquetaProducto->fill($request->all());
        $etiquetaProducto->save();

        Flash::success('Etiqueta Producto Actualizado.');

        return redirect(route('etiquetaProductos.index'));
    }

    /**
     * Remove the specified EtiquetaProducto from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            Flash::error('Etiqueta Producto No encontrado');

            return redirect(route('etiquetaProductos.index'));
        }

        $etiquetaProducto->delete();

        Flash::success('Etiqueta Producto Eliminado.');

        return redirect(route('etiquetaProductos.index'));
    }
}
