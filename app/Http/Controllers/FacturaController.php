<?php

namespace App\Http\Controllers;

use App\DataTables\FacturaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFacturaRequest;
use App\Http\Requests\UpdateFacturaRequest;
use App\Models\Factura;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FacturaController extends AppBaseController
{
    /**
     * Display a listing of the Factura.
     *
     * @param FacturaDataTable $facturaDataTable
     * @return Response
     */
    public function index(FacturaDataTable $facturaDataTable)
    {
        return $facturaDataTable->render('facturas.index');
    }

    /**
     * Show the form for creating a new Factura.
     *
     * @return Response
     */
    public function create()
    {
        return view('facturas.create');
    }

    /**
     * Store a newly created Factura in storage.
     *
     * @param CreateFacturaRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturaRequest $request)
    {
        $input = $request->all();

        /** @var Factura $factura */
        $factura = Factura::create($input);

        Flash::success('Factura Guardado.');

        return redirect(route('facturas.index'));
    }

    /**
     * Display the specified Factura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            Flash::error('Factura No encontrado');

            return redirect(route('facturas.index'));
        }

        return view('facturas.show')->with('factura', $factura);
    }

    /**
     * Show the form for editing the specified Factura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            Flash::error('Factura No encontrado');

            return redirect(route('facturas.index'));
        }

        return view('facturas.edit')->with('factura', $factura);
    }

    /**
     * Update the specified Factura in storage.
     *
     * @param  int              $id
     * @param UpdateFacturaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaRequest $request)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            Flash::error('Factura No encontrado');

            return redirect(route('facturas.index'));
        }

        $factura->fill($request->all());
        $factura->save();

        Flash::success('Factura Actualizado.');

        return redirect(route('facturas.index'));
    }

    /**
     * Remove the specified Factura from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            Flash::error('Factura No encontrado');

            return redirect(route('facturas.index'));
        }

        $factura->delete();

        Flash::success('Factura Eliminado.');

        return redirect(route('facturas.index'));
    }
}
