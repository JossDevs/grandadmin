<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAtributosProductoRequest;
use App\Http\Requests\UpdateAtributosProductoRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\AtributosProducto;
use Illuminate\Http\Request;
use Flash;
use Response;

class AtributosProductoController extends AppBaseController
{
    /**
     * Display a listing of the AtributosProducto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var AtributosProducto $atributosProductos */
        $atributosProductos = AtributosProducto::paginate(10);

        return view('atributos_productos.index')
            ->with('atributosProductos', $atributosProductos);
    }

    /**
     * Show the form for creating a new AtributosProducto.
     *
     * @return Response
     */
    public function create()
    {
        return view('atributos_productos.create');
    }

    /**
     * Store a newly created AtributosProducto in storage.
     *
     * @param CreateAtributosProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateAtributosProductoRequest $request)
    {
        $input = $request->all();

        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::create($input);

        Flash::success('Atributos Producto Guardado.');

        return redirect(route('atributosProductos.index'));
    }

    /**
     * Display the specified AtributosProducto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            Flash::error('Atributos Producto No encontrado');

            return redirect(route('atributosProductos.index'));
        }

        return view('atributos_productos.show')->with('atributosProducto', $atributosProducto);
    }

    /**
     * Show the form for editing the specified AtributosProducto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            Flash::error('Atributos Producto No encontrado');

            return redirect(route('atributosProductos.index'));
        }

        return view('atributos_productos.edit')->with('atributosProducto', $atributosProducto);
    }

    /**
     * Update the specified AtributosProducto in storage.
     *
     * @param int $id
     * @param UpdateAtributosProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAtributosProductoRequest $request)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            Flash::error('Atributos Producto No encontrado');

            return redirect(route('atributosProductos.index'));
        }

        $atributosProducto->fill($request->all());
        $atributosProducto->save();

        Flash::success('Atributos Producto Actualizado.');

        return redirect(route('atributosProductos.index'));
    }

    /**
     * Remove the specified AtributosProducto from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            Flash::error('Atributos Producto No encontrado');

            return redirect(route('atributosProductos.index'));
        }

        $atributosProducto->delete();

        Flash::success('Atributos Producto Eliminado.');

        return redirect(route('atributosProductos.index'));
    }
}
