<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFacturaAPIRequest;
use App\Http\Requests\API\UpdateFacturaAPIRequest;
use App\Models\Factura;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\FacturaResource;
use Response;

/**
 * Class FacturaController
 * @package App\Http\Controllers\API
 */

class FacturaAPIController extends AppBaseController
{
    /**
     * Display a listing of the Factura.
     * GET|HEAD /facturas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Factura::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $facturas = $query->get();

        return $this->sendResponse(FacturaResource::collection($facturas), 'Facturas retrieved successfully');
    }

    /**
     * Store a newly created Factura in storage.
     * POST /facturas
     *
     * @param CreateFacturaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Factura $factura */
        $factura = Factura::create($input);

        return $this->sendResponse(new FacturaResource($factura), 'Factura Guardado');
    }

    /**
     * Display the specified Factura.
     * GET|HEAD /facturas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            return $this->sendError('Factura No encontrado');
        }

        return $this->sendResponse(new FacturaResource($factura), 'Factura retrieved successfully');
    }

    /**
     * Update the specified Factura in storage.
     * PUT/PATCH /facturas/{id}
     *
     * @param int $id
     * @param UpdateFacturaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaAPIRequest $request)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            return $this->sendError('Factura No encontrado');
        }

        $factura->fill($request->all());
        $factura->save();

        return $this->sendResponse(new FacturaResource($factura), 'Factura Actualizado');
    }

    /**
     * Remove the specified Factura from storage.
     * DELETE /facturas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Factura $factura */
        $factura = Factura::find($id);

        if (empty($factura)) {
            return $this->sendError('Factura No encontrado');
        }

        $factura->delete();

        return $this->sendSuccess('Factura Eliminado');
    }
}
