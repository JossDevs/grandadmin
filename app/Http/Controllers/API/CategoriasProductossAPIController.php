<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoriasProductossAPIRequest;
use App\Http\Requests\API\UpdateCategoriasProductossAPIRequest;
use App\Models\CategoriasProductoss;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CategoriasProductossResource;
use Response;

/**
 * Class CategoriasProductossController
 * @package App\Http\Controllers\API
 */

class CategoriasProductossAPIController extends AppBaseController
{
    /**
     * Display a listing of the CategoriasProductoss.
     * GET|HEAD /categoriasProductosses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = CategoriasProductoss::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $categoriasProductosses = $query->get();

        return $this->sendResponse(CategoriasProductossResource::collection($categoriasProductosses), 'Categorias Productosses retrieved successfully');
    }

    /**
     * Store a newly created CategoriasProductoss in storage.
     * POST /categoriasProductosses
     *
     * @param CreateCategoriasProductossAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriasProductossAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoriasProductoss $categoriasProductoss */
        $categoriasProductoss = CategoriasProductoss::create($input);

        return $this->sendResponse(new CategoriasProductossResource($categoriasProductoss), 'Categorias Productoss Guardado');
    }

    /**
     * Display the specified CategoriasProductoss.
     * GET|HEAD /categoriasProductosses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoriasProductoss $categoriasProductoss */
        $categoriasProductoss = CategoriasProductoss::find($id);

        if (empty($categoriasProductoss)) {
            return $this->sendError('Categorias Productoss No encontrado');
        }

        return $this->sendResponse(new CategoriasProductossResource($categoriasProductoss), 'Categorias Productoss retrieved successfully');
    }

    /**
     * Update the specified CategoriasProductoss in storage.
     * PUT/PATCH /categoriasProductosses/{id}
     *
     * @param int $id
     * @param UpdateCategoriasProductossAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriasProductossAPIRequest $request)
    {
        /** @var CategoriasProductoss $categoriasProductoss */
        $categoriasProductoss = CategoriasProductoss::find($id);

        if (empty($categoriasProductoss)) {
            return $this->sendError('Categorias Productoss No encontrado');
        }

        $categoriasProductoss->fill($request->all());
        $categoriasProductoss->save();

        return $this->sendResponse(new CategoriasProductossResource($categoriasProductoss), 'CategoriasProductoss Actualizado');
    }

    /**
     * Remove the specified CategoriasProductoss from storage.
     * DELETE /categoriasProductosses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoriasProductoss $categoriasProductoss */
        $categoriasProductoss = CategoriasProductoss::find($id);

        if (empty($categoriasProductoss)) {
            return $this->sendError('Categorias Productoss No encontrado');
        }

        $categoriasProductoss->delete();

        return $this->sendSuccess('Categorias Productoss Eliminado');
    }
}
