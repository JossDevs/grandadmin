<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoriaAPIRequest;
use App\Http\Requests\API\UpdateCategoriaAPIRequest;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CategoriaResource;
use Response;

/**
 * Class CategoriaController
 * @package App\Http\Controllers\API
 */

class CategoriaAPIController extends AppBaseController
{
    /**
     * Display a listing of the Categoria.
     * GET|HEAD /categorias
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Categoria::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $categorias = $query->get();

        return $this->sendResponse(CategoriaResource::collection($categorias), 'Categorias retrieved successfully');
    }

    /**
     * Store a newly created Categoria in storage.
     * POST /categorias
     *
     * @param CreateCategoriaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Categoria $categoria */
        $categoria = Categoria::create($input);

        return $this->sendResponse(new CategoriaResource($categoria), 'Categoria Guardado');
    }

    /**
     * Display the specified Categoria.
     * GET|HEAD /categorias/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Categoria $categoria */
        $categoria = Categoria::find($id);

        if (empty($categoria)) {
            return $this->sendError('Categoria No encontrado');
        }

        return $this->sendResponse(new CategoriaResource($categoria), 'Categoria retrieved successfully');
    }

    /**
     * Update the specified Categoria in storage.
     * PUT/PATCH /categorias/{id}
     *
     * @param int $id
     * @param UpdateCategoriaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriaAPIRequest $request)
    {
        /** @var Categoria $categoria */
        $categoria = Categoria::find($id);

        if (empty($categoria)) {
            return $this->sendError('Categoria No encontrado');
        }

        $categoria->fill($request->all());
        $categoria->save();

        return $this->sendResponse(new CategoriaResource($categoria), 'Categoria Actualizado');
    }

    /**
     * Remove the specified Categoria from storage.
     * DELETE /categorias/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Categoria $categoria */
        $categoria = Categoria::find($id);

        if (empty($categoria)) {
            return $this->sendError('Categoria No encontrado');
        }

        $categoria->delete();

        return $this->sendSuccess('Categoria Eliminado');
    }
}
