<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoriaProductosAPIRequest;
use App\Http\Requests\API\UpdateCategoriaProductosAPIRequest;
use App\Models\CategoriaProductos;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CategoriaProductosResource;
use Response;

/**
 * Class CategoriaProductosController
 * @package App\Http\Controllers\API
 */

class CategoriaProductosAPIController extends AppBaseController
{
    /**
     * Display a listing of the CategoriaProductos.
     * GET|HEAD /categoriaProductos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = CategoriaProductos::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $categoriaProductos = $query->get();

        return $this->sendResponse(CategoriaProductosResource::collection($categoriaProductos), 'Categoria Productos retrieved successfully');
    }

    /**
     * Store a newly created CategoriaProductos in storage.
     * POST /categoriaProductos
     *
     * @param CreateCategoriaProductosAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriaProductosAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoriaProductos $categoriaProductos */
        $categoriaProductos = CategoriaProductos::create($input);

        return $this->sendResponse(new CategoriaProductosResource($categoriaProductos), 'Categoria Productos Guardado');
    }

    /**
     * Display the specified CategoriaProductos.
     * GET|HEAD /categoriaProductos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoriaProductos $categoriaProductos */
        $categoriaProductos = CategoriaProductos::find($id);

        if (empty($categoriaProductos)) {
            return $this->sendError('Categoria Productos No encontrado');
        }

        return $this->sendResponse(new CategoriaProductosResource($categoriaProductos), 'Categoria Productos retrieved successfully');
    }

    /**
     * Update the specified CategoriaProductos in storage.
     * PUT/PATCH /categoriaProductos/{id}
     *
     * @param int $id
     * @param UpdateCategoriaProductosAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriaProductosAPIRequest $request)
    {
        /** @var CategoriaProductos $categoriaProductos */
        $categoriaProductos = CategoriaProductos::find($id);

        if (empty($categoriaProductos)) {
            return $this->sendError('Categoria Productos No encontrado');
        }

        $categoriaProductos->fill($request->all());
        $categoriaProductos->save();

        return $this->sendResponse(new CategoriaProductosResource($categoriaProductos), 'CategoriaProductos Actualizado');
    }

    /**
     * Remove the specified CategoriaProductos from storage.
     * DELETE /categoriaProductos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoriaProductos $categoriaProductos */
        $categoriaProductos = CategoriaProductos::find($id);

        if (empty($categoriaProductos)) {
            return $this->sendError('Categoria Productos No encontrado');
        }

        $categoriaProductos->delete();

        return $this->sendSuccess('Categoria Productos Eliminado');
    }
}
