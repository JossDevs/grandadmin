<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoriaProductoAPIRequest;
use App\Http\Requests\API\UpdateCategoriaProductoAPIRequest;
use App\Models\CategoriaProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CategoriaProductoResource;
use Response;

/**
 * Class CategoriaProductoController
 * @package App\Http\Controllers\API
 */

class CategoriaProductoAPIController extends AppBaseController
{
    /**
     * Display a listing of the CategoriaProducto.
     * GET|HEAD /categoriaProductos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = CategoriaProducto::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $categoriaProductos = $query->get();

        return $this->sendResponse(CategoriaProductoResource::collection($categoriaProductos), 'Categoria Productos retrieved successfully');
    }

    /**
     * Store a newly created CategoriaProducto in storage.
     * POST /categoriaProductos
     *
     * @param CreateCategoriaProductoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriaProductoAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::create($input);

        return $this->sendResponse(new CategoriaProductoResource($categoriaProducto), 'Categoria Producto Guardado');
    }

    /**
     * Display the specified CategoriaProducto.
     * GET|HEAD /categoriaProductos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            return $this->sendError('Categoria Producto No encontrado');
        }

        return $this->sendResponse(new CategoriaProductoResource($categoriaProducto), 'Categoria Producto retrieved successfully');
    }

    /**
     * Update the specified CategoriaProducto in storage.
     * PUT/PATCH /categoriaProductos/{id}
     *
     * @param int $id
     * @param UpdateCategoriaProductoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriaProductoAPIRequest $request)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            return $this->sendError('Categoria Producto No encontrado');
        }

        $categoriaProducto->fill($request->all());
        $categoriaProducto->save();

        return $this->sendResponse(new CategoriaProductoResource($categoriaProducto), 'CategoriaProducto Actualizado');
    }

    /**
     * Remove the specified CategoriaProducto from storage.
     * DELETE /categoriaProductos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoriaProducto $categoriaProducto */
        $categoriaProducto = CategoriaProducto::find($id);

        if (empty($categoriaProducto)) {
            return $this->sendError('Categoria Producto No encontrado');
        }

        $categoriaProducto->delete();

        return $this->sendSuccess('Categoria Producto Eliminado');
    }
}
