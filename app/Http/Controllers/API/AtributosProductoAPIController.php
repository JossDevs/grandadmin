<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAtributosProductoAPIRequest;
use App\Http\Requests\API\UpdateAtributosProductoAPIRequest;
use App\Models\AtributosProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AtributosProductoResource;
use Response;

/**
 * Class AtributosProductoController
 * @package App\Http\Controllers\API
 */

class AtributosProductoAPIController extends AppBaseController
{
    /**
     * Display a listing of the AtributosProducto.
     * GET|HEAD /atributosProductos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = AtributosProducto::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $atributosProductos = $query->get();

        return $this->sendResponse(AtributosProductoResource::collection($atributosProductos), 'Atributos Productos retrieved successfully');
    }

    /**
     * Store a newly created AtributosProducto in storage.
     * POST /atributosProductos
     *
     * @param CreateAtributosProductoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAtributosProductoAPIRequest $request)
    {
        $input = $request->all();

        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::create($input);

        return $this->sendResponse(new AtributosProductoResource($atributosProducto), 'Atributos Producto Guardado');
    }

    /**
     * Display the specified AtributosProducto.
     * GET|HEAD /atributosProductos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            return $this->sendError('Atributos Producto No encontrado');
        }

        return $this->sendResponse(new AtributosProductoResource($atributosProducto), 'Atributos Producto retrieved successfully');
    }

    /**
     * Update the specified AtributosProducto in storage.
     * PUT/PATCH /atributosProductos/{id}
     *
     * @param int $id
     * @param UpdateAtributosProductoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAtributosProductoAPIRequest $request)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            return $this->sendError('Atributos Producto No encontrado');
        }

        $atributosProducto->fill($request->all());
        $atributosProducto->save();

        return $this->sendResponse(new AtributosProductoResource($atributosProducto), 'AtributosProducto Actualizado');
    }

    /**
     * Remove the specified AtributosProducto from storage.
     * DELETE /atributosProductos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AtributosProducto $atributosProducto */
        $atributosProducto = AtributosProducto::find($id);

        if (empty($atributosProducto)) {
            return $this->sendError('Atributos Producto No encontrado');
        }

        $atributosProducto->delete();

        return $this->sendSuccess('Atributos Producto Eliminado');
    }
}
