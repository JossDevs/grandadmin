<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEtiquetaProductoAPIRequest;
use App\Http\Requests\API\UpdateEtiquetaProductoAPIRequest;
use App\Models\EtiquetaProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\EtiquetaProductoResource;
use Response;

/**
 * Class EtiquetaProductoController
 * @package App\Http\Controllers\API
 */

class EtiquetaProductoAPIController extends AppBaseController
{
    /**
     * Display a listing of the EtiquetaProducto.
     * GET|HEAD /etiquetaProductos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = EtiquetaProducto::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $etiquetaProductos = $query->get();

        return $this->sendResponse(EtiquetaProductoResource::collection($etiquetaProductos), 'Etiqueta Productos retrieved successfully');
    }

    /**
     * Store a newly created EtiquetaProducto in storage.
     * POST /etiquetaProductos
     *
     * @param CreateEtiquetaProductoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEtiquetaProductoAPIRequest $request)
    {
        $input = $request->all();

        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::create($input);

        return $this->sendResponse(new EtiquetaProductoResource($etiquetaProducto), 'Etiqueta Producto Guardado');
    }

    /**
     * Display the specified EtiquetaProducto.
     * GET|HEAD /etiquetaProductos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            return $this->sendError('Etiqueta Producto No encontrado');
        }

        return $this->sendResponse(new EtiquetaProductoResource($etiquetaProducto), 'Etiqueta Producto retrieved successfully');
    }

    /**
     * Update the specified EtiquetaProducto in storage.
     * PUT/PATCH /etiquetaProductos/{id}
     *
     * @param int $id
     * @param UpdateEtiquetaProductoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEtiquetaProductoAPIRequest $request)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            return $this->sendError('Etiqueta Producto No encontrado');
        }

        $etiquetaProducto->fill($request->all());
        $etiquetaProducto->save();

        return $this->sendResponse(new EtiquetaProductoResource($etiquetaProducto), 'EtiquetaProducto Actualizado');
    }

    /**
     * Remove the specified EtiquetaProducto from storage.
     * DELETE /etiquetaProductos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EtiquetaProducto $etiquetaProducto */
        $etiquetaProducto = EtiquetaProducto::find($id);

        if (empty($etiquetaProducto)) {
            return $this->sendError('Etiqueta Producto No encontrado');
        }

        $etiquetaProducto->delete();

        return $this->sendSuccess('Etiqueta Producto Eliminado');
    }
}
