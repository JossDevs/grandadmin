<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrdenProductoAPIRequest;
use App\Http\Requests\API\UpdateOrdenProductoAPIRequest;
use App\Models\OrdenProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OrdenProductoResource;
use Response;

/**
 * Class OrdenProductoController
 * @package App\Http\Controllers\API
 */

class OrdenProductoAPIController extends AppBaseController
{
    /**
     * Display a listing of the OrdenProducto.
     * GET|HEAD /ordenProductos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = OrdenProducto::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $ordenProductos = $query->get();

        return $this->sendResponse(OrdenProductoResource::collection($ordenProductos), 'Orden Productos retrieved successfully');
    }

    /**
     * Store a newly created OrdenProducto in storage.
     * POST /ordenProductos
     *
     * @param CreateOrdenProductoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenProductoAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::create($input);

        return $this->sendResponse(new OrdenProductoResource($ordenProducto), 'Orden Producto Guardado');
    }

    /**
     * Display the specified OrdenProducto.
     * GET|HEAD /ordenProductos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            return $this->sendError('Orden Producto No encontrado');
        }

        return $this->sendResponse(new OrdenProductoResource($ordenProducto), 'Orden Producto retrieved successfully');
    }

    /**
     * Update the specified OrdenProducto in storage.
     * PUT/PATCH /ordenProductos/{id}
     *
     * @param int $id
     * @param UpdateOrdenProductoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenProductoAPIRequest $request)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            return $this->sendError('Orden Producto No encontrado');
        }

        $ordenProducto->fill($request->all());
        $ordenProducto->save();

        return $this->sendResponse(new OrdenProductoResource($ordenProducto), 'OrdenProducto Actualizado');
    }

    /**
     * Remove the specified OrdenProducto from storage.
     * DELETE /ordenProductos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            return $this->sendError('Orden Producto No encontrado');
        }

        $ordenProducto->delete();

        return $this->sendSuccess('Orden Producto Eliminado');
    }
}
