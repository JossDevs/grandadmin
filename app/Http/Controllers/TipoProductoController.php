<?php

namespace App\Http\Controllers;

use App\DataTables\TipoProductoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipoProductoRequest;
use App\Http\Requests\UpdateTipoProductoRequest;
use App\Models\TipoProducto;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TipoProductoController extends AppBaseController
{
    /**
     * Display a listing of the TipoProducto.
     *
     * @param TipoProductoDataTable $tipoProductoDataTable
     * @return Response
     */
    public function index(TipoProductoDataTable $tipoProductoDataTable)
    {
        return $tipoProductoDataTable->render('tipo_productos.index');
    }

    /**
     * Show the form for creating a new TipoProducto.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_productos.create');
    }

    /**
     * Store a newly created TipoProducto in storage.
     *
     * @param CreateTipoProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoProductoRequest $request)
    {
        $input = $request->all();

        /** @var TipoProducto $tipoProducto */
        $tipoProducto = TipoProducto::create($input);

        Flash::success('Tipo Producto Guardado.');

        return redirect(route('tipoProductos.index'));
    }

    /**
     * Display the specified TipoProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TipoProducto $tipoProducto */
        $tipoProducto = TipoProducto::find($id);

        if (empty($tipoProducto)) {
            Flash::error('Tipo Producto No encontrado');

            return redirect(route('tipoProductos.index'));
        }

        return view('tipo_productos.show')->with('tipoProducto', $tipoProducto);
    }

    /**
     * Show the form for editing the specified TipoProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var TipoProducto $tipoProducto */
        $tipoProducto = TipoProducto::find($id);

        if (empty($tipoProducto)) {
            Flash::error('Tipo Producto No encontrado');

            return redirect(route('tipoProductos.index'));
        }

        return view('tipo_productos.edit')->with('tipoProducto', $tipoProducto);
    }

    /**
     * Update the specified TipoProducto in storage.
     *
     * @param  int              $id
     * @param UpdateTipoProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoProductoRequest $request)
    {
        /** @var TipoProducto $tipoProducto */
        $tipoProducto = TipoProducto::find($id);

        if (empty($tipoProducto)) {
            Flash::error('Tipo Producto No encontrado');

            return redirect(route('tipoProductos.index'));
        }

        $tipoProducto->fill($request->all());
        $tipoProducto->save();

        Flash::success('Tipo Producto Actualizado.');

        return redirect(route('tipoProductos.index'));
    }

    /**
     * Remove the specified TipoProducto from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TipoProducto $tipoProducto */
        $tipoProducto = TipoProducto::find($id);

        if (empty($tipoProducto)) {
            Flash::error('Tipo Producto No encontrado');

            return redirect(route('tipoProductos.index'));
        }

        $tipoProducto->delete();

        Flash::success('Tipo Producto Eliminado.');

        return redirect(route('tipoProductos.index'));
    }
}
