<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenProductoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenProductoRequest;
use App\Http\Requests\UpdateOrdenProductoRequest;
use App\Models\OrdenProducto;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenProductoController extends AppBaseController
{
    /**
     * Display a listing of the OrdenProducto.
     *
     * @param OrdenProductoDataTable $ordenProductoDataTable
     * @return Response
     */
    public function index(OrdenProductoDataTable $ordenProductoDataTable)
    {
        return $ordenProductoDataTable->render('orden_productos.index');
    }

    /**
     * Show the form for creating a new OrdenProducto.
     *
     * @return Response
     */
    public function create()
    {
        return view('orden_productos.create');
    }

    /**
     * Store a newly created OrdenProducto in storage.
     *
     * @param CreateOrdenProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenProductoRequest $request)
    {
        $input = $request->all();

        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::create($input);

        Flash::success('Orden Producto Guardado.');

        return redirect(route('ordenProductos.index'));
    }

    /**
     * Display the specified OrdenProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            Flash::error('Orden Producto No encontrado');

            return redirect(route('ordenProductos.index'));
        }

        return view('orden_productos.show')->with('ordenProducto', $ordenProducto);
    }

    /**
     * Show the form for editing the specified OrdenProducto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            Flash::error('Orden Producto No encontrado');

            return redirect(route('ordenProductos.index'));
        }

        return view('orden_productos.edit')->with('ordenProducto', $ordenProducto);
    }

    /**
     * Update the specified OrdenProducto in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenProductoRequest $request)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            Flash::error('Orden Producto No encontrado');

            return redirect(route('ordenProductos.index'));
        }

        $ordenProducto->fill($request->all());
        $ordenProducto->save();

        Flash::success('Orden Producto Actualizado.');

        return redirect(route('ordenProductos.index'));
    }

    /**
     * Remove the specified OrdenProducto from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrdenProducto $ordenProducto */
        $ordenProducto = OrdenProducto::find($id);

        if (empty($ordenProducto)) {
            Flash::error('Orden Producto No encontrado');

            return redirect(route('ordenProductos.index'));
        }

        $ordenProducto->delete();

        Flash::success('Orden Producto Eliminado.');

        return redirect(route('ordenProductos.index'));
    }
}
