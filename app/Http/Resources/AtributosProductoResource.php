<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AtributosProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'Nombre' => $this->Nombre,
            'Slug' => $this->Slug,
            'TipoProductoId' => $this->TipoProductoId,
            'OrdenProductoId' => $this->OrdenProductoId,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
