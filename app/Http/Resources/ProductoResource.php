<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'Nombre' => $this->Nombre,
            'Descripcion' => $this->Descripcion,
            'Precio' => $this->Precio,
            'Impuesto' => $this->Impuesto,
            'Categoria' => $this->Categoria,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
