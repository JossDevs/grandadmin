<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FacturaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fecha_emision' => $this->fecha_emision,
            'estado' => $this->estado,
            'total' => $this->total,
            'iva' => $this->iva,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
