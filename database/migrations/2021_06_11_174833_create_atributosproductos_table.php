<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAtributosproductosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atributosproductos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Nombre');
            $table->string('Slug');
            $table->integer('TipoProductoId');
            $table->integer('OrdenProductoId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atributosproductos');
    }
}
