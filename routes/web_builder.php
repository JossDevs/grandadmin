<?php

use Illuminate\Support\Facades\Route;


Route::resource('tipoProductos', 'TipoProductoController');

Route::resource('ordenProductos', 'OrdenProductoController');

Route::resource('atributosProductos', 'AtributosProductoController');

Route::resource('etiquetaProductos', 'EtiquetaProductoController');

Route::resource('categoriaProductos', 'CategoriaProductoController');

Route::resource('productos', 'ProductoController');

Route::resource('facturas', 'FacturaController');